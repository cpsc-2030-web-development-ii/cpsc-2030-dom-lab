let fname=document.getElementById("fname");
let lname=document.getElementById("lname");
let email=document.getElementById("email");
let sales=document.getElementById("sales");
let errorDiv=document.getElementById("errmsg");
let table=document.querySelector("table");
let tbody=document.querySelector("tbody");
let updateButton=document.getElementById("update");
let lastSelectedRow;

table.setAttribute("class", "table table-hover");
updateButton.setAttribute("disabled", "");
document.getElementById("add").onclick = addRow;
document.getElementById("update").onclick=updateRow;
fname.focus();

function updateRow(){
"use strict";
  if (validInput()){
    lastSelectedRow.cells[0].textContent=fname.value;
    lastSelectedRow.cells[1].textContent=lname.value;
    lastSelectedRow.cells[2].textContent=email.value;
    lastSelectedRow.cells[3].textContent=sales.value;
    emptyFields();
  }
}

function addRow(){
"use strict";

  if (validInput()){
    let tableRow=document.createElement("tr");
    let tableDatafn=document.createElement("td");
    let tableDataln=document.createElement("td");
    let tableDataem=document.createElement("td");
    let tableDatasl=document.createElement("td");
    let tableDatabtn=document.createElement("td");
    let buttonRow=document.createElement("button");

    tableDatafn.textContent=fname.value;
    tableDataln.textContent=lname.value;
    tableDataem.textContent=email.value;
    tableDatasl.textContent=sales.value;
    tableDatasl.setAttribute("class", "text-right");
    buttonRow.setAttribute("class", "btn btn-danger");
    buttonRow.textContent='Delete';
    buttonRow.onclick = function(event) {
      event.stopPropagation();
      tbody.removeChild(tableRow);
      }

    tableRow.appendChild(tableDatafn);
    tableRow.appendChild(tableDataln);
    tableRow.appendChild(tableDataem);
    tableRow.appendChild(tableDatasl);
    tableDatabtn.appendChild(buttonRow);
    tableRow.appendChild(tableDatabtn);
    tableRow.onclick = function(){
      selectRow(tableRow);      
    };
    tbody.appendChild(tableRow);
    emptyFields();
    fname.focus();
  }
}

function selectRow(tableRow){
"use strict";
/*console.log("function selectRow -> On click");
console.log("target nodeName:"+event.target.nodeName);
console.log(typeof lastSelectedRow);*/
if (typeof lastSelectedRow!=="undefined") {
   if (lastSelectedRow!==tableRow) {
    lastSelectedRow.removeAttribute("class");
  }
}
lastSelectedRow=tableRow;
tableRow.setAttribute("class", "table-active");
fname.value=tableRow.cells[0].textContent;
lname.value=tableRow.cells[1].textContent;
email.value=tableRow.cells[2].textContent;
sales.value=tableRow.cells[3].textContent;
updateButton.removeAttribute("disabled");
}

function validInput(){
"use strict";
let errorMsg="";
  if (fname.value==""){
    errorMsg+="The First Name is a mandatory field!"+"<br>";
  }
  if (lname.value==""){
    errorMsg+="The Last Name is a mandatory field!"+"<br>";
  }
  if (email.value==""){
    errorMsg+="The Email is a mandatory field!"+"<br>";
  }
  if (sales.value==""){
    errorMsg+="The Monthly Sales is a mandatory field!"+"<br>";
    } else if (isNaN(sales.value)){
      errorMsg+="Monthly Sales must to be a number!"+"<br>";
      } else if (sales.value<0){
        errorMsg+="Monthly Sales field requires a positive value!"+"<br>";
      }
      
  if (errorMsg!==""){
    errorDiv.firstElementChild.innerHTML=errorMsg;
    errorDiv.removeAttribute("class", "d-none");
  
  } else {
    errorDiv.setAttribute("class", "d-none");
    }

  return(errorMsg=="");
}
function emptyFields(){
"use strict"
  fname.value="";
  lname.value="";
  email.value="";
  sales.value="";
  updateButton.setAttribute("disabled", "");
}